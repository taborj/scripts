# scripts

A collection of useful scripts, generally for various *nix systems.

- rotate.sh: auto rename files for backups (i.e. tar.gz.0, tar.gz.1, etc)
- dumpDB.sh: dump all databases from a MySQL/MariaDB database server, then tar.gz them