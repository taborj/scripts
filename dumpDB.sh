#!/usr/local/bin/bash

# Dump databases from MySQL/MariaDB,
# tar them up and save them off

# (C) 2019 Jon Tabor   taborj@obsolete.site

# The path to mysqldump and the mysql client
binPath='/usr/local/bin'

# A place to temporarily store the exported database files
exportPath='/local/path/for/tmp/usage'

# The final resting place of the tar.gz file containing the
# exported databases
savePath='/backup/location'


if [ -z $1 ] || [ -z $2 ]
then
	echo "Usage: dumpDB.sh [user] [password]"
	exit 0
fi

dbList=($(echo "show databases;" | $binPath/mysql -u $1 -p$2)) 
for i in "${dbList[@]}"; do 
	echo "Dumping database $i..."
	$binPath/mysqldump --skip-lock-tables -u $1 -p$2 $i > $exportPath/$i.sql; 
done

echo "tar-ing files..."
tar czf $savePath/all_databases.tar.gz *.sql *.sh 
