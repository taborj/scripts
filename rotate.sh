#!/usr/local/bin/bash

# Rotate files ending in a number,
# i.e. file.tar.gz.0 to file.tar.gz.1, etc
#
# (C) 2019 Jon Tabor  taborj@obsolete.site

if [ -z $1 ] || [ -z $2 ]
then
	echo "Usage: rotate.sh [max count] [base filename]"
	echo "  e.g. rotate.sh 10 file.tar.gz"
	echo ""
	exit 0
fi

maxCount=$1
baseFile=$2

counter=$maxCount
until [ $counter -lt 0 ]
do
	if [ $counter -eq 0 ]
	then
		if [ -e $baseFile ]
		then
			mv $baseFile $baseFile.$counter
		fi
	else
		if [ -e $baseFile.$(($counter-1)) ]
		then
			mv $baseFile.$(($counter-1)) $baseFile.$counter
		fi
	fi
	((counter--))
done
